-- Main.hs
-- Functional projekt FLP, dka-2-mka, 8.2.2022
-- Author: Matěj Kudera, xkuder04, FIT
-- Main project file

------- Imports --------
import System.Environment ( getArgs )
import System.IO (hPutStrLn, stderr)
import Text.Read (readMaybe)
import Data.Maybe (fromJust, isNothing, isJust)
import System.Directory ( doesFileExist )
import Data.List ( (\\), sort )
import GHC.IO.Exception ( ExitCode(ExitFailure) )
import System.Exit ( exitWith )

-------- Types ----------
data Automaton = Automaton { states :: [Int]
                            , alphabet :: [Char]
                            , initialState :: Int
                            , finalStates :: [Int]
                            , rules :: [Rule]
                            }

data Rule = Rule { startState :: Int
                  , character :: Char
                  , endState :: Int
                 } deriving (Show, Ord, Eq)

--------------- Printing --------------------
-- Show instance used to print automaton in desired form
instance Show Automaton where
    show l = printStates (states l) ++ "\n" ++ alphabet l ++ "\n" ++ show (initialState l) ++ printFinalStatesAndRules (finalStates l) (rules l)

-- Print states in format given by assignment
printStates :: Show a => [a] -> [Char]
printStates statesLst = tail (foldr (\state acm -> "," ++ show state ++ acm ) [] statesLst)

-- Print rules in format given by assignment
printRules :: [Rule] -> [Char]
printRules rulesLst = init (foldr (\rule acm -> printRule rule ++ acm) [] rulesLst)

printRule :: Rule -> [Char]
printRule rule = show (startState rule) ++ "," ++ [character rule] ++ "," ++ show (endState rule) ++ "\n"

-- Dont print finalStates and/or Rules if they dont exists in automaton
printFinalStatesAndRules :: Show a => [a] -> [Rule] -> [Char]
printFinalStatesAndRules finStates rulesLst
  | null finStates && null rulesLst = ""
  | null rulesLst = "\n" ++ printStates finStates
  | null finStates = "\n" ++ printRules rulesLst
  | otherwise = "\n" ++ printStates finStates ++ "\n" ++ printRules rulesLst

--------------- Error messages ----------------
exitWithErrorMessage :: String -> ExitCode -> IO a
exitWithErrorMessage str e = hPutStrLn stderr str >> exitWith e

---------------- Input control -----------------
-- Get first item if exists
getFirst :: [[a]] -> [a]
getFirst [] = []
getFirst (x:_) = x

-- Check if file given in args exists
checkFileExists :: FilePath -> IO ()
checkFileExists file = do
   fileExist <- doesFileExist file

   if not fileExist then
    exitWithErrorMessage "File doesn't exist" (ExitFailure 1)
   else
    putStr ""

-- Read input from file or stdin
readInput :: [Char] -> IO String
readInput [] = getContents
readInput a = readFile a

-- Split string by delimiter
splitBy :: Eq a => a -> [a] -> [[a]]
splitBy _ [] = []
splitBy delim xs = takeWhile (delim /=) xs : splitBy delim (drop 1 (dropWhile (delim /=) xs))

-- Convert state names to integer
convertStatesToInt :: [String] -> [Maybe Int]
convertStatesToInt [] = []
convertStatesToInt xs = map readMaybe xs

-- Check if states have format given by assignment
checkStates :: [Int] -> Bool
checkStates [] = True
checkStates (x:xs) = (x>=0) && notElem x xs && checkStates xs

-- Check if aphabet has format given by assignment
checkAlphabet :: [Char] -> Bool
checkAlphabet [] = True
checkAlphabet (x:xs) = (x>='a') && (x<='z') && notElem x xs && checkAlphabet xs

-- Check if list contains rules or final states
checkIfFinalStates :: [Maybe Int] -> Bool
checkIfFinalStates = notElem Nothing

-- If list dont look like final states, final states dont exist
selectFinalStatesList :: [Maybe Int] -> [Maybe Int]
selectFinalStatesList list = if checkIfFinalStates list then list else []

-- Convert final states from maybe to int
convertStatesFromJust :: [Maybe a] -> [a]
convertStatesFromJust [] = []
convertStatesFromJust xs = map fromJust xs

-- Check if final states exists in states list
checkFinalStates :: Eq a => [a] -> [a] -> Bool
checkFinalStates [] _ = True
checkFinalStates (x:xs) lst = elem x lst && checkFinalStates xs lst

-- Determin rest of input from final states list
selectRestOfInput :: [Maybe Int] -> [[Char]] -> [[Char]]
selectRestOfInput finalStatesLst input = if null finalStatesLst then input else tail input

-- Check if rules consist of 3 parts, 0 if rules where not given
checkLenghtOfRules :: [[a]] -> Bool
checkLenghtOfRules [] = True
checkLenghtOfRules [[]] = True
checkLenghtOfRules (x:xs) = (null x || (length x == 3)) && checkLenghtOfRules xs

-- Check if states in rules are Int
rulesStatesMaybe :: [[[Char]]] -> [(Maybe Int, String, Maybe Int)]
rulesStatesMaybe rulesLst = if rulesLst == [[]] then [] else map (\(x:y:z:_) -> (readMaybe x, y, readMaybe z)) rulesLst

-- Convert rules states to int
rulesStatesInt :: [(Maybe a, b, Maybe c)] -> [(a, b, c)]
rulesStatesInt = map (\item-> (fromJust (get1st item), get2nd item, fromJust (get3rd item)))

-- Convert transition string to char
rulesMakeChar :: [(Int, String , Int)] -> [(Int, Char, Int)]
rulesMakeChar = map (\item-> (get1st item, head (get2nd item), get3rd item))

-- Check if states are defined and characters are defined
checkIfRulesOk :: [(Int, Char, Int)] -> [Int] -> String  -> Bool
checkIfRulesOk lstOfRules lstStates lstAlphabet = foldr (\item acm -> elem (get1st item) lstStates && elem (get2nd item) lstAlphabet && elem (get3rd item) lstStates && acm) True lstOfRules

-- Check if duplicities exists in list
checkDuplicities :: Eq a => [a] -> Bool
checkDuplicities [] = False
checkDuplicities (x:xs) = (x `elem` xs) || checkDuplicities xs

-- Get elements from tuple
get1st :: (a, b, c) -> a
get1st (a,_,_) = a

get2nd :: (a, b, c) -> b
get2nd (_,a,_) = a

get3rd :: (a, b, c) -> c
get3rd (_,_,a) = a

---------------- Algorithms -----------------
-- Calculates reachable states iterativeli until calculations i and i-1 are same
-- Lists must be sorted otherwise equal sing does not work
getReachableStates :: Automaton -> [Int] -> [Int]
getReachableStates automaton lst = if newlst == lst then lst else getReachableStates automaton newlst
    where newlst = reachableStatesStep automaton lst

-- Aplication of algorithm step for reachable states calculation
reachableStatesStep :: Automaton -> [Int] -> [Int]
reachableStatesStep automaton lst = sort (removeDuplicities (lst ++ foldr (\rule acm -> if startState rule `elem` lst then endState rule : acm else acm) [] (rules automaton)))

-- Remove duplicities in list
removeDuplicities :: Eq a => [a] -> [a]
removeDuplicities [] = []
removeDuplicities (x:xs) = if x `elem` xs then removeDuplicities xs else x : removeDuplicities xs

-- Get max item in list
getListMax :: [Int] -> Int
getListMax [] = 0
getListMax [a] = a
getListMax (x:y:xs) = getListMax ((if x>y then x else y):xs)

-- Algoruthm to get indistinguishable states of automaton
getIndistinguishableStates :: Automaton -> [(Int, Int)] -> [(Int, Int)]
getIndistinguishableStates automaton lst = if newlst == lst then lst else getIndistinguishableStates automaton newlst
    where newlst = indistinguishableStatesStep automaton lst

-- Calculate step of algorithm for indistinguishable states
indistinguishableStatesStep :: Automaton -> [(Int, Int)] -> [(Int, Int)]
indistinguishableStatesStep automaton lst = sort (foldr (\tpl acm -> if uncurry checkTuplesInList tpl automaton lst then tpl:acm else acm) [] lst)

-- Check if all tuples exists in old list of tuples
checkTuplesInList :: Int -> Int -> Automaton -> [(Int, Int)] -> Bool
checkTuplesInList a b automaton lst = foldr (\tupl acm -> (tupl `elem` lst) && acm) True (getTuples a b automaton)

-- Makes list of tuples from end states of rules
getTuples :: Int -> Int -> Automaton -> [(Int, Int)]
getTuples a b automaton = sort (map
  (\ charc
     -> (getEndStateOfRule a charc (rules automaton),
         getEndStateOfRule b charc (rules automaton))) (alphabet automaton))

-- Gets end state from rule defined by start state and character
getEndStateOfRule :: Int -> Char -> [Rule] -> Int
getEndStateOfRule st charc rulesLst = head (foldr (\rule acm -> if startState rule == st && character rule == charc then endState rule:acm else acm) [] rulesLst)

-- Get list of equivalent states with given state from tuple list
getEquivalentStates :: Eq a1 => a1 -> [(a1, a2)] -> [a2]
getEquivalentStates st = foldr (\tpl acm -> if st == fst tpl then snd tpl:acm else acm) []

-- Add state number to equivalence group
addStateNumber :: Int -> Int -> [[Int]] -> [(Int, [Int])]
addStateNumber _ _ [] = []
addStateNumber initState num (x:xs) = if initState `elem` x then (0, x):addStateNumber initState num xs else (num, x):addStateNumber initState (num+1) xs

-- Map correct state numbers to temporary numbers
mapCorrectStateNumbers :: Int -> Int -> [(Int, Int)] -> [(Int, Char, Int)] -> [(Int, Int)]
mapCorrectStateNumbers maxNum mapNum mapLst rulesLst = if getStateToChange maxNum rulesLst == -1 then mapLst else mapCorrectStateNumbers maxNum (mapNum + 1) ((getStateToChange maxNum rulesLst, mapNum):mapLst) (updateRulesTuples (getStateToChange maxNum rulesLst) mapNum rulesLst)

-- Get rules tuples updated with new state number
updateRulesTuples :: Int -> Int -> [(Int, Char, Int)] -> [(Int, Char, Int)]
updateRulesTuples oldNum newNum rulesLst = sort (foldr (\ruleTpl acm -> if (get1st ruleTpl == oldNum) && (get3rd ruleTpl == oldNum) then (newNum, get2nd ruleTpl, newNum):acm else if get1st ruleTpl == oldNum then (newNum, get2nd ruleTpl, get3rd ruleTpl):acm else if get3rd ruleTpl == oldNum then (get1st ruleTpl, get2nd ruleTpl, newNum):acm else ruleTpl:acm) [] rulesLst)

-- Get end state of first rule tuple to change
getStateToChange :: Int -> [(Int, Char, Int)] -> Int
getStateToChange _ [] = -1
getStateToChange maxNum (x:xs) = if get3rd x >= maxNum then get3rd x else getStateToChange maxNum xs

-- Check if at least one item of first list exists in secont list
checkListItemInList :: Eq a => [a] -> [a] -> Bool
checkListItemInList itemLst lst = foldr (\item acm -> elem item lst || acm) False itemLst

-- Get equivalence group from state number
getEqGroup :: [(Int, [Int])] -> Int -> [Int]
getEqGroup eqGroups st = head (foldr (\eqGroup acm -> if st == fst eqGroup then snd eqGroup:acm else acm) [] eqGroups)

-- Returns number of equivalence group with specified state
getState :: [(Int, [Int])] -> Int  -> Int
getState eqGroups st = head (foldr (\eqGroup acm -> if st `elem` snd eqGroup then fst eqGroup:acm else acm) [] eqGroups)

-- Get end state of mka
getEndState :: Int -> Char -> [(Int, [Int])] -> [Rule] -> Int
getEndState startSt chrc eqGroups oldRules = getState eqGroups (getEndStateOfRule (head (getEqGroup eqGroups startSt)) chrc oldRules)

--------------------- Main --------------------
main :: IO ()
main = do
    -- Check arguments --
    args <- getArgs

    if length args /= 2 && length args /= 1 then
        exitWithErrorMessage "Program is used with following argumnets.\n\nflp21-fun selection [input]\nwhere:\nselection = -i to print loaded automaton in internal representation or -t to print created MKA\ninput = name of file containing DKA that will be converted, if not provided DKA is loaded from stdin" (ExitFailure 1)
    else
        putStr ""

    let switch = head args
    let inputFile = getFirst (tail args)

    if switch /= "-i" && switch /= "-t" then
        exitWithErrorMessage "Program is used with following argumnets.\n\nflp21-fun selection [input]\nwhere:\nselection = -i to print loaded automaton in internal representation or -t to print created MKA\ninput = name of file containing DKA that will be converted, if not provided DKA is loaded from stdin" (ExitFailure 1)
    else
        putStr ""

    -- If file provided check if exists
    if not(null inputFile) then
        checkFileExists inputFile
    else
        putStr ""

    -- Read input from given file or stdin
    inputString <- readInput inputFile

    -- Remove empty lines
    let inputStringNoEmpty = unlines (filter (/= "") (lines inputString))
    -- Remove spaces
    let cleanInputString = filter (/= ' ') inputStringNoEmpty

    -- Load input string into internal representation
    -- States --
    -- Check if states given in input
    let inputByLines = lines cleanInputString
    if null inputByLines then
        exitWithErrorMessage "Automatons states not given" (ExitFailure 1)
    else
        putStr ""

    -- Devide input into states and rest
    let automatonStates = head inputByLines
    let inputAlphabetPart = tail inputByLines

    -- Split states string to get list with states
    let statesSplited = splitBy ',' automatonStates

    -- Check if states are Int
    let statesListMaybe = convertStatesToInt statesSplited

    if Nothing `elem` statesListMaybe then
        exitWithErrorMessage "Bad states format" (ExitFailure 1)
    else
        putStr ""

    -- Convert states from Just to Int
    let statesList = map fromJust statesListMaybe

    -- Check if states have good format
    if checkStates statesList then
        putStr ""
    else
        exitWithErrorMessage "Bad states format" (ExitFailure 1)

    -- Alphabet --
    -- Check if aplhabet given in input
    if null inputAlphabetPart then
        exitWithErrorMessage "Automatons alphabet not given" (ExitFailure 1)
    else
        putStr ""

    -- Devide input into aplhabet and rest
    let automatonAlphabet = head inputAlphabetPart
    let inputInitialStatePart= tail inputAlphabetPart

    -- Check if alphabet has good format
    if checkAlphabet automatonAlphabet then
        putStr ""
    else
        exitWithErrorMessage "Bad states format" (ExitFailure 1)

    -- Initial state --
    -- Check if initial state given in input
    if null inputInitialStatePart then
        exitWithErrorMessage "Automatons initial state not given" (ExitFailure 1)
    else
        putStr ""

    -- Devide input into initial state and rest
    let automatonInitialState = head inputInitialStatePart
    let inputFinalStatesPart= tail inputInitialStatePart

    -- Check if initial state is Int
    let maybeIntialState = readMaybe automatonInitialState :: Maybe Int

    if isNothing maybeIntialState then
        exitWithErrorMessage "Bad initial state format" (ExitFailure 1)
    else
        putStr ""

    -- Convert initial state in Just to normal Int
    let initState = fromJust maybeIntialState

    -- Check if initial state is in states
    if initState `notElem` statesList then
        exitWithErrorMessage "Initial state not in states" (ExitFailure 1)
    else
        putStr ""

    -- Final states --
    -- Check if final states part exists in input
    let automatonFinalStates = getFirst inputFinalStatesPart

    -- If string format is bad, input is probably rule from next section
    let finalStatesSplited = splitBy ',' automatonFinalStates
    let finalStatesListMaybe = convertStatesToInt finalStatesSplited

    -- Devide input into final states and rest
    let finalStatesList = selectFinalStatesList finalStatesListMaybe
    let inputRulesPart = selectRestOfInput finalStatesList inputFinalStatesPart

    let finStates = convertStatesFromJust finalStatesList

    -- Check if final states are in states
    if checkFinalStates finStates statesList then
        putStr ""
    else
        exitWithErrorMessage "Final state not in states" (ExitFailure 1)

    -- Check duplicities
    if checkDuplicities finStates then
        exitWithErrorMessage "Duplicate final states" (ExitFailure 1)
    else
        putStr ""

    -- Rules --
    -- Check rules format
    let rulesSplited = map (splitBy ',') inputRulesPart

    if not (checkLenghtOfRules rulesSplited) then
        exitWithErrorMessage "Bad rules format" (ExitFailure 1)
    else
        putStr ""

    -- Check integers
    let rulesMaybe = rulesStatesMaybe rulesSplited
    let statesAreInt = foldr (\item acc -> isJust (get1st item) && isJust (get3rd item) && acc) True rulesMaybe

    if not statesAreInt then
        exitWithErrorMessage "Bad rules format" (ExitFailure 1)
    else
        putStr ""

    let rulesWithInt = rulesStatesInt rulesMaybe

    -- Check if transition string has length 1 (is char)
    if not (foldr (\item acm -> length (get2nd item) == 1 && acm) True rulesWithInt) then
        exitWithErrorMessage "Bad rules format" (ExitFailure 1)
    else
        putStr ""

    -- Convert string in tuple to char
    let rulesWithChar = rulesMakeChar rulesWithInt

    -- Check if states and characters exists
    if not (checkIfRulesOk rulesWithChar statesList automatonAlphabet) then
        exitWithErrorMessage "Bad rules format" (ExitFailure 1)
    else
        putStr ""

    -- Check if automaton is deterministic
    let startStateAndCharTuples = map (\tpl -> (get1st tpl, get2nd tpl)) rulesWithChar

    if checkDuplicities startStateAndCharTuples then
        exitWithErrorMessage "Automaton is not deterministic" (ExitFailure 1)
    else
        putStr ""

    -- Create internal representation of MKA
    let rulesList = map (\item -> Rule (get1st item) (get2nd item) (get3rd item)) rulesWithChar
    let dka = Automaton {states = statesList, alphabet = automatonAlphabet, initialState = initState, finalStates = finStates, rules = rulesList}

    ------------ Convert dka to mka ------------
    -- Remove unreachable states --
    -- Get reachable and unreachable states
    let reachableStates = getReachableStates dka [initialState dka]
    --let unreachableStates = [x | x <- states dka, x `notElem` reachableStates]

    -- Select rules to keep based on reachable states
    let rulesToKeep = foldr (\rule acm -> if startState rule `elem` reachableStates then rule:acm else acm) [] (rules dka)
    let finalStatesToKeep = foldr (\finalState acm -> if finalState `elem` reachableStates then finalState:acm else acm) [] (finalStates dka)

    -- Make new internal representation of changed automaton
    let dkaReachable = Automaton {states = reachableStates, alphabet = automatonAlphabet, initialState = initState, finalStates = finalStatesToKeep, rules = rulesToKeep}

    -- Make automaton whole --
    -- Make startState and character tuples form rules
    let rulesTuples = sort (map (\rule -> (startState rule, character rule)) (rules dkaReachable))

    -- Make tuples that should be in whole automaton
    let automatonTuples = sort ([(a,b) | a <- states dkaReachable, b <- alphabet dkaReachable])

    -- Determine which rules should be added
    let missingRules = automatonTuples \\ rulesTuples

    -- Add new state if automaton is not whole
    let newState = getListMax (states dkaReachable) + 1
    let wholeAutomatonStates = sort (if null missingRules then states dkaReachable else newState:states dkaReachable)

    -- Determine new rules
    let wholeAutomatonTuples = sort ([(a,b) | a <- wholeAutomatonStates, b <- alphabet dkaReachable])
    let tuplesToAdd = wholeAutomatonTuples \\ rulesTuples

    -- Create new rules and make whole automaton
    let newRules = rules dkaReachable ++ map (\tpl -> uncurry Rule tpl newState) tuplesToAdd
    let dkaWhole = Automaton {states = wholeAutomatonStates, alphabet = automatonAlphabet, initialState = initState, finalStates = finalStatesToKeep, rules = newRules}

    -- Run algorithm to make mka --
    -- Get tuples of final states and rest of states
    let finalStatesTuples = [(a,b) | a <- finalStates dkaWhole, b <- finalStates dkaWhole]
    let nonFinalStatesTuples = [(a,b) | a <- states dkaWhole \\ finalStates dkaWhole, b <- states dkaWhole \\ finalStates dkaWhole]

    -- Get tuples of indistinguishable states
    let indistinguishableStates = getIndistinguishableStates dkaWhole (finalStatesTuples ++ nonFinalStatesTuples)

    -- Make lists of equivalent states
    let equivalentStates = removeDuplicities (map (`getEquivalentStates` indistinguishableStates) (states dkaWhole))

    -- ///////////////////////////////////////
    -- Make new states for mka
    -- Initial state is alwais 0
    -- Created states are indexed from 0
    -- End state of first rule can be only 0 or 1, end state of other rules must be maximally greater by one then maximal rule before it (sorted in lexikographic order)
    -- ////////////////////////////////////////

    -- Write temporary state number into tuple with equivalence group
    -- Initial state gest number 0
    -- Other states get number count of new states
    let equivalenceGroupsWithNumber = addStateNumber (initialState dkaWhole) (length  equivalentStates) equivalentStates

    -- Create temporary states and rules lists
    let temporaryStates = map fst equivalenceGroupsWithNumber
    let temporaryRuleTuples = sort [(startStm, chrc, getEndState startStm chrc equivalenceGroupsWithNumber (rules dkaWhole)) | startStm <- temporaryStates, chrc <- alphabet dkaWhole]

    -- Map state number to state number that is dictated by given state numbering rules
    let stateNumberMap = mapCorrectStateNumbers (length  equivalentStates) 1 [(0,0)] temporaryRuleTuples

    -- Create assign new numbers to equivalence groups
    let equivalenceGroupsWithStateNum = zip (map snd (sort stateNumberMap)) (map snd (sort equivalenceGroupsWithNumber))

    -- Get states, init state and final states of mka
    let mkaStates = map fst equivalenceGroupsWithStateNum
    let mkaFinalStates = foldr (\equTupl acm -> if checkListItemInList (finalStates dkaWhole) (snd equTupl) then fst equTupl:acm else acm) [] equivalenceGroupsWithStateNum

    -- Make rules for mka
    let mkaRules = [Rule startStm chrc  (getEndState startStm chrc equivalenceGroupsWithStateNum (rules dkaWhole)) | startStm <- mkaStates, chrc <- alphabet dkaWhole]

    -- Sort states in increasing order
    -- Sort aplhabet in alphabetical order
    -- Sort rules in lexikographic order (start state, character)
    let mka = Automaton {states = sort mkaStates, alphabet = sort automatonAlphabet, initialState = 0, finalStates = sort mkaFinalStates, rules = sort mkaRules}

    -- Print automaton
    if "-i" == switch then
        print dka
    else if "-t" == switch then
        print mka
    else
        putStr ""

-- END Main.hs