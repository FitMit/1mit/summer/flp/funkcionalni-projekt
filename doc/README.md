Funkcionální projekt FLP 2021/2022
===============
Autor: Matěj Kudera <br>
Stručný popis činnosti funkcionálního projektu do předmětu FLP.

Použití
---
Program se spouští s následujícímy parametry:
```
flp21-fun prepinac [vstupni_soubor]
```
Přepínačem se volí zda se má na výstup vypsat načtený dka = -i, který si program uložil do vstupní reprezentace nebo se má na vstup vypsat vytvořený mka = -t. Parametr vstupního souboru očekává cestu k souboru, ve kterém je uložen dka, tento parametr však není povinný, pokud tento parametr není zadaný očekává se, že dka bude vložený na standartním vstupu programu.

Implementace
---
Činnost programu začíná kontrolou vstupních parametrů. Očekává se, že bude zadán buď jeden parametr a nebo dva parametry. Pokud je zadaný jiný počet parametrů tak program končí a vypíše na chybový výstup jednoduchou nápovědu jak má být program použit. Také se kontroluje, že první parametr, určující který automat má být vypsán na standartní výstup, nabývá pouze hodnot -i a nebo -t, které jsou definovány zadáním a pokud tomu tak není, tak je také vypsána jednoduchá nápověda. Pokud je zadán druhý parametr, který určuje soubor ze vstupním automatem, tak se testuje zda zadaný soubor existuje, pokud tomu tak není je tato skutečnost uživateli vypsána a program končí.

Dále se ze souboru nebo ze standartního vstupu, načte vstupní řetězec a provedou se na něm kontroly, které určí zda je obsah načteného řetězce doopravdy automat ve formátu specifikovaném zadáním. Kontrola začíná tím, že se řetězec rozdělí na jednotlivé řádky. Dále jsou odstraněny prázdné řádky a ze zbylých řádků jsou odstraněny zbytečné mezery. Na takto upraveném vstupu se následně kontorluje zda jednotlivé řádky obsahují správné části automatu dle zadání a zda jsou ve správném formátu:

- Na prvním řádku mají být uvedeny stavy automatu odděleny ",". Řádek je tedy rozdělen podle "," a kontroluje se zda jsou odělené řetězce čísla >= 0. Kontroluje se také zda nebyly zadány duplicitní stavy. Pokud program zjistí chybu ve formatů nebo nalezne duplicitný stavy, tak je vypsána příslušná chyba a program končí.
- Na druhém řádku by měla být uvedena abeceda. Program zde kontroluje zda jsou na řádku uvedeny znaky v rozmezí "a" - "z" a zda zde nejsou přítomny žádné duplicity.
- Na třetím řádku by se měl nacházet počáteční stav. Zde se kontroluje zda řádek obsahuje jedno číslo a zda je toto číslo uvedeno ve stavech automatu.
- Čtvrtý řádek být měl obsahovat množinu koncových stavů ve stejném formátu jako je množina stavů na prvním řádku. Zde se může nastat, že automat neobsahuje žádný koncový stav a tento řádek by byl prázdný. Pokud se tedy zjistí, že řetězec na řádku není v očekávaném formátu tak se bere, že neexistuje žádný koncový stav a tento řádek obsahuje už informace k pravidlům automatu a tento řádek se nezpracovává. Pokud řetězec na tomto řádku vyhoví kontrolám jak pro první řádek, tak se navíc kontroluje zda jsou zadané koncové stavy obsaženy mezi stavy automatu zadanými na prvním řádku.
- Zbyké řádky vstupu by měly obsahovat pravidla pro zadaný automat. Pokud už však na vstupu nejsou žádné řádky tak se bere, že automat nemá žádná pravidla. Jinak se na každém řádku kontroluje zda má řetězec formát dle zadání a to "aktualni_stav,symbol_abecedy,nový_stav". Každý řetězec je tedy rozdělen podle "," a testuje se zda je první a poslední řetězec číslo a zda je toto číslo obsaženo mezi stavy automatu. U prostředního symbolu se testuje zda je obsažen v abecedě automatu. Je zde také testováno, že automat neobsahuje více pravidel se stejným aktuálním stavem a symbolem, pokud by toto nastalo tak by vstupní automat nebyl deterministický a nevyhovoval by zadání.

Pokud program při postupném kontrolování řádků zjistý, že není uveden řádek s informací nezbytně nutnou pro vytvoření automatu, tak program skončí s chybou udávající co ve vstupním souboru chybí.

Zkotrolovaný vstup je uložen do vytvořených datových struktur, které automat uchovává během celého procesu jeho převádění z podoby dka na mka.

Dále jsou na načteném a uloženém automatu prováděny algoritmy, které ho převedou do minimální podoby (mka). Prvně jsou z automatu odstraněny nedostupné stavy podle algoritmu z opory (Algoritmus 3.4). Dále je potřeba, aby byl automat úplný, toto se testuje tak, že se vytvoří seznam dvojic vytvořený ze všech pravidel automatu, kde každá dvojice má tvar (aktualni_stav,symbol). Pak se vytvoří druhý seznam dvojic z množiny stavů a abecedy automatu (stav,symbol). Pokud od druhého saznamu dvojic odečteme první seznam dvojic a výsledek je prázdný je automat úplný. Jinak pro plnost automatu musíme přidat jeden nový stav, pro který budou všechna pravidla směřovat zpátky do tohoto stavu a dále musíme přidat všechna pravidla, která určují dvojice zbylé po odečtení seznamů. Každá dvojice z tohoto seznamu určuje pro nové pravidla auktuální stav, symbol a jako nový stav se vezme ten stav, který jsme aktuálně vytvořili. Nakonec je na automatu proveden algoritmus z opory (algortimus 3.5), který nalezne třídy ekvivalence, které následně reprezentují stavy minimálního automatu. 

Po dokončení minimalizace jsou nalezené třídy ekvivalence uloženy v pomocném poli, kde je dále potřeba jim přiřadit taková čísla stavů, které odpovídají požadavkům v zadání. Počátečním stavem se stává třída ekvivalence, která obsahuje původní počáteční stav v dka a dostává přiřazené číslo 0. Ostatním třídám ekvivalence jsou čísla stavů přiřazena tak, aby měly větší hodnoty než jaký je počet stavů minimálníhho automatu. Toto je využito k tomu, aby se následně mohly tyto nekoncové stavy přemapovat na čísla 1 až počet stavů minimálního automatu mínus jedna, jak to očekává zadání. Mapování se zde provádí podle dalšího požadavku zadání a to tak, že ve výsledních pravidlech minimálního automatu mají být pouze pravidla takového tvaru, kde cílový stav prvního pravidla může být jen 0 nebo 1 a cílový stav každého dalšího pravidla smí být nejvýše o 1 větší než největší stav vyskytující se v pravidlech nad ním. Za účelem splnění tohoto pokynu zadání byla vygenerována přechodová pravidla pro vytvořený minimální automat s aktuálními čísly stavů a iterativně je vždy v těchto seřazených pravidlech hledáno první pravidlo, kde jeho koncový stav není v rozsahu specifikovaném zadáním a pak je číslo tohoto stavu namapována na novou zatím nepoužitou minimální hodnotu. Pak se v pomocných pravidlech nová hodnota aktualizuje a pravidla se seřadí. Takto se postupně mapují všechny čísla stavů na správné dle zadání. Díky tomuto mapování stavů už je jednoduché vytvořit finální minimální automat dle požadavků zadání:

- Stavy automatu mají čísla 0 až počet stavů mínus jedna
- Abeceda je stejná jak u dka
- Počteční stav je 0
- Ostatní stavy mají čísla dle vytvořené mapovací tabulky
- Koncové stavy jsou ty stavy, kde jejich ekvivalenční třída obsahuje nějaký koncový stav z původního dka
- Pravidla minimálního automatu jsou vytvořena pomocí původních přechodových pravidle pro jednotlivé stavy v dka a podle namapovaných čísel stavů

Všechny toto vytvořené hodnoty jsou ještě finálně seřazeny podle požadavků v zadání a následně uloženy do vnitřní reprezentace.

Na konci programu je na standartním výstupu vypsán ten automat, který byl vybrát parametrem.

Testy
---
K vytvořenému programu jsou ve složce tests přiloženy základní testy a skript v jazyce Python, který tyto testy spouští. Testy zde kontrolují zda dokáže vytvořený program správně odhalit vstupy se špatným formátem a zda dokáže správně převést zadaný automat do jeho minimální podoby.

Testy lze spustit přes program make, kde je potřeba prvně program přeložit *make* a pak se testy spustí příkazem *make test*.