# Makefile
# Functional project FLP, dka-2-mka, 8.2.2022
# Author: Matěj Kudera, xkuder04, FIT
# Makefile for translation

######### Variables ########
HS = ghc
FLAGS_HS = -Wall

######## Build ########
all: flp21-fun

flp21-fun: src/Main.hs
	$(HS) $(FLAGS_HS) src/Main.hs -o flp21-fun

############ Comands ############
# Run tests
test:
	python tests/tester.py

# Clear repositary
clean:
	rm -f flp21-fun flp-fun-xkuder04.zip src/*.o src/*.hi

# Make zip
zip: 
	zip flp-fun-xkuder04.zip doc/* src/*.hs tests/* Makefile
