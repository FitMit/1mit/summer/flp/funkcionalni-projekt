FLP - Funkcionální projekt
---------

Program provádějící transformaci deterministických konečných automatů na minimální deterministické konečné automaty (DKA-2-MKA).

Body: 8/12

K programu nebylo poskytnuto žádné písemné hodnocení, takže není jasné za co byly strženy body.