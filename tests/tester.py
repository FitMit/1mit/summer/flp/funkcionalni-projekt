# Tester.py
# Functional project FLP, dka-2-mka, 8.2.2022
# Author: Matej Kudera, xkuder04, FIT
# Automated tester script

#### Imports ####
import subprocess
import os

# Test actual directory
cwd = os.getcwd()
act_directory = cwd.split('/')[-1]

# Make relative path to test directory if needed
pwd_tests = ""
pwd_program = ""
if act_directory != "tests":
    pwd_tests += "tests/"
    pwd_program += "./flp21-fun"
else:
    pwd_tests += "."
    pwd_program += "../flp21-fun"

# Iterate through tests
passed_tests = 0
tests = 0
for file in os.listdir(pwd_tests):
    if file.endswith(".in"):
        tests += 1

        # Get test files
        input_file = file
        output_file = file[:-3] + ".out"
        print("Test: " + input_file)

        # Setup process
        pwd_input_file = os.path.join(pwd_tests, input_file)
        pwd_output_file = os.path.join(pwd_tests, output_file)

        bashCmd = [pwd_program, "-t", pwd_input_file]
        process = subprocess.Popen(bashCmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Check result
        output, error = process.communicate()

        first_line = ""
        with open(pwd_output_file) as f:
            first_line = f.readline().rstrip()

        if first_line == "ERROR":
            if process.returncode  == 1:
                print("Test passed")
                passed_tests += 1
                continue
        else:
            expected_output = ""
            with open(pwd_output_file) as f:
                expected_output = f.read()

            if output == expected_output:
                print("Test passed")
                passed_tests += 1
                continue

        print("Test failed")

print("Passed "+str(passed_tests)+"/"+str(tests))